lazy val akkaHttpVersion = "10.0.11"
lazy val akkaVersion    = "2.5.11"
lazy val logbackVersion   = "1.2.3"
lazy val twitter4jVersion = "4.0.6"
lazy val scalaGuiceVersion = "4.1.1"
lazy val scalaLoggingVersion = "3.8.0"
lazy val scalaTestVersion = "3.0.5"
lazy val slickVersion = "3.2.0"
lazy val flywayVersion = "5.0.7"
lazy val mariaDBClientVersion = "2.2.2"

lazy val `twitter-stream-api` = (project in file(".")).enablePlugins(FlywayPlugin).enablePlugins(JavaAppPackaging).
  settings(
    inThisBuild(List(
      organization    := "io.twitteranalytics",
      version         := "0.1-SNAPSHOT",
      scalaVersion    := "2.12.5"
    )),
    name := "twitter-stream-api",
    libraryDependencies ++= Seq(
      "com.typesafe.akka"   %% "akka-actor"               % akkaVersion,
      "com.typesafe.akka"   %% "akka-http"                % akkaHttpVersion,
      "com.typesafe.akka"   %% "akka-http-spray-json"     % akkaHttpVersion,
      "com.typesafe.akka"   %% "akka-stream"              % akkaVersion,
      "com.typesafe.akka"   %% "akka-slf4j"               % akkaVersion,
      "com.typesafe.slick"  %% "slick"                    % slickVersion,
      "com.typesafe.slick"  %% "slick-hikaricp"           % slickVersion,
      "org.mariadb.jdbc"    % "mariadb-java-client"       % mariaDBClientVersion,
      "ch.qos.logback"      % "logback-classic"           % logbackVersion,
      "org.twitter4j"       % "twitter4j-stream"          % twitter4jVersion,
      "org.flywaydb"        % "flyway-core"               % flywayVersion,
      "net.codingwell"      %% "scala-guice"              % scalaGuiceVersion,
      "com.typesafe.scala-logging"  %% "scala-logging"    % scalaLoggingVersion,
      "com.github.tototoshi" %% "slick-joda-mapper"       % "2.3.0",
      "joda-time" % "joda-time"                           % "2.7",
      "org.joda" % "joda-convert"                         % "1.7",
      "com.typesafe.akka" %% "akka-stream-kafka" % "0.19",

      "com.typesafe.akka"   %% "akka-http-testkit"        % akkaHttpVersion   % Test,
      "com.typesafe.akka"   %% "akka-testkit"             % akkaVersion       % Test,
      "com.typesafe.akka"   %% "akka-stream-testkit"      % akkaVersion       % Test,
      "org.scalatest"       %% "scalatest"                % scalaTestVersion  % Test
    )
  )

unmanagedResourceDirectories in Compile += {
  baseDirectory.value / "src/main/resources"
}