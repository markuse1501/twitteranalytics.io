import com.typesafe.config.ConfigFactory


val parsedConfig = ConfigFactory.parseFile(new File("src/main/resources/application.conf"))
val conf = ConfigFactory.load(parsedConfig)

flywayUrl := Option(conf.getString("streams-db.properties.url")).getOrElse("")
flywayUser := Option(conf.getString("streams-db.properties.user")).getOrElse("")
flywayPassword := Option(conf.getString("streams-db.properties.password")).getOrElse("")
flywayLocations += Option(conf.getString("streams-db.migrationPath")).getOrElse("")
flywayTable := "schema_version"