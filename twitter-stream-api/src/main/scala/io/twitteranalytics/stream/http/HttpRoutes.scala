package io.twitteranalytics.stream.http

import akka.http.scaladsl.server.Directives.{get, pathPrefix, _}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import javax.inject.{Inject, Singleton}

@Singleton
class HttpRoutes @Inject()(streamRestApi: TwitterStreamApi) {

  val all: Route = {
    pathPrefix("v1") {
      streamRestApi.routes
    } ~
      pathPrefix("health") {
        get {
          complete("OK")
        }
      }
  }
}
