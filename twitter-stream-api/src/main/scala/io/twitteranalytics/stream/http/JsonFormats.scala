package io.twitteranalytics.stream.http

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import io.twitteranalytics.stream.actor.StreamManager.ProcessorCountResult
import io.twitteranalytics.stream.db.SaveStreamRequest
import io.twitteranalytics.stream.{CreateStreamRequest, StreamProcessorRequest, TwitterStream, UpdateStreamRequest}
import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormatter, ISODateTimeFormat}
import spray.json.{DefaultJsonProtocol, _}

import scala.util.Try

trait JsonFormats extends SprayJsonSupport with DefaultJsonProtocol {

  implicit object DateTimeFormat extends RootJsonFormat[DateTime] {

    val parser: DateTimeFormatter = ISODateTimeFormat.dateTimeParser()

    def write(obj: DateTime): JsValue = {
      JsString(ISODateTimeFormat.basicDateTime.print(obj))
    }

    def read(json: JsValue): DateTime = json match {
      case JsString(s) => Try(parser.parseDateTime(s)).getOrElse(error(s))
      case _ => error(json.toString())
    }

    def error(v: Any): DateTime = {
      deserializationError(
        s"""
           |'$v' is not a valid date value. Dates must be in format:
           |     * date-opt-time     = date-element ['T' [time-element] [offset]]
           |     * date-element      = std-date-element | ord-date-element | week-date-element
           |     * std-date-element  = yyyy ['-' MM ['-' dd]]
           |     * ord-date-element  = yyyy ['-' DDD]
           |     * week-date-element = xxxx '-W' ww ['-' e]
           |     * time-element      = HH [minute-element] | [fraction]
           |     * minute-element    = ':' mm [second-element] | [fraction]
           |     * second-element    = ':' ss [fraction]
           |     * offset            = 'Z' | (('+' | '-') HH [':' mm [':' ss [('.' | ',') SSS]]])
           |     * fraction          = ('.' | ',') digit+
        """.stripMargin
      )
    }
  }

  implicit val inputStreamFormats = jsonFormat9(TwitterStream)
  implicit val saveStreamFormats = jsonFormat6(SaveStreamRequest)
  implicit val createStreamFormats = jsonFormat5(CreateStreamRequest)
  implicit val updateStreamFormats = jsonFormat6(UpdateStreamRequest)

  implicit val streamProcessorRequestFormats = jsonFormat2(StreamProcessorRequest)
  implicit val processorCountFormats = jsonFormat1(ProcessorCountResult)



}
