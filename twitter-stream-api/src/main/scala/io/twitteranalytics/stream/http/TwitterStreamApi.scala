package io.twitteranalytics.stream.http

import akka.actor.ActorSelection
import akka.http.scaladsl.model.StatusCodes.{InternalServerError, NotFound, OK}
import akka.http.scaladsl.server.Directives.{pathPrefix, _}
import akka.http.scaladsl.server.directives.MethodDirectives.get
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.http.scaladsl.server.{ExceptionHandler, Route}
import akka.pattern.ask
import akka.util.Timeout
import com.google.inject.name.Named
import com.typesafe.scalalogging.LazyLogging
import io.twitteranalytics.stream.actor.StreamManager.{CreateStream, GetProcessors, ProcessorCountResult, StartStream}
import io.twitteranalytics.stream.db.StreamService
import io.twitteranalytics.stream.{CreateStreamRequest, StreamProcessorRequest, TwitterStream, UpdateStreamRequest}
import javax.inject.Singleton

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.util.{Failure, Success}

@Singleton
class TwitterStreamApi(streamService: StreamService, @Named("stream-manager") streamManager: ActorSelection)
                      (implicit executionContext: ExecutionContext)
  extends JsonFormats with LazyLogging {

  private implicit val timeout = Timeout(5 seconds)

  val routes: Route =
    handleExceptions(exceptionHandler) {
      all ~ byHash ~ create ~ update ~ remove ~ start ~ processors
    }

  def exceptionHandler: ExceptionHandler = ExceptionHandler {
    case ex: Exception =>
      logger.error(s"Handle Exception: [${ex.getClass.getSimpleName}: ${ex.getMessage}]")
      complete(InternalServerError, ex.getMessage)
  }

  // GET /streams
  def all: Route =
    pathPrefix("streams") {
      pathEndOrSingleSlash {
        get {
          onComplete(streamService.getStreams) {
            case Success(streams) =>
              logger.info(s"Fetched all streams: ${streams.size}")
              complete(OK, streams)
            case Failure(ex) =>
              logger.error(s"Failure: ${ex.getMessage}")
              complete(InternalServerError, ex.getMessage)
          }
        }
      }
    }


  // GET /streams/:hash
  def byHash: Route = {
    pathPrefix("streams" / Segment) { hash =>
      pathEndOrSingleSlash {
        get {
          onComplete(streamService.getStream(hash)) {
            case Success(optStream) =>
              optStream match {
                case Some(stream) =>
                  logger.info(s"Fetched stream: ${stream.toString}")
                  complete(OK, stream)
                case None =>
                  logger.info(s"Stream '$hash' not found")
                  complete(NotFound, s"Stream '$hash' not found")
              }
            case Failure(ex) =>
              logger.error(s"Failure: ${ex.getMessage}")
              complete(InternalServerError, ex.getMessage)
          }
        }
      }
    }
  }

  // POST /streams
  def create: Route = {
    pathPrefix("streams") {
      pathEndOrSingleSlash {
        post {
          entity(as[CreateStreamRequest]) { request =>
            onComplete(streamService.createStream(request)) {
              case Success(stream) =>
                logger.info(s"Created stream: ${stream.toString}")
                //create stream in manager
                (streamManager ? CreateStream(stream))
                  .mapTo[String]
                  .map { hash =>
                    logger.info(s"Registered '$hash' in StreamManager")
                  }
                complete(OK, stream)
              case Failure(ex) =>
                logger.error(s"Failure: ${ex.getMessage}")
                complete(InternalServerError, ex.getMessage)
            }
          }
        }
      }
    }
  }

  // PUT /streams/:hash
  def update: Route = {
    pathPrefix("streams" / Segment) {
      hash =>
        pathEndOrSingleSlash {
          put {
            entity(as[UpdateStreamRequest]) { request =>
              onComplete(streamService.updateStream(hash, request)) {
                case Success(stream) => complete(OK, stream)
                case Failure(ex) => complete(InternalServerError, ex.getMessage)
              }
            }
          }
        }
    }
  }

  // DELETE /streams/:hash
  def remove: Route = {
    pathPrefix("streams" / Segment) {
      hash =>
        pathEndOrSingleSlash {
          delete {
            onComplete(streamService.deleteStream(hash)) {
              case Success(rows) => complete(OK, s"$rows streams deleted")
              case Failure(ex) => complete(InternalServerError, ex.getMessage)
            }
          }
        }
    }
  }

  def start: Route = {
    pathPrefix("streams" / Segment) { hash =>
      pathEndOrSingleSlash {
        put {
          entity(as[StreamProcessorRequest]) { request => //TODO
            logger.info(s"Received HTTP request: ${request.toString}")
            onComplete((streamManager ? StartStream(hash)).mapTo[Option[TwitterStream]]) {
              case Success(optStream) =>
                optStream match {
                  case Some(stream) => complete(OK, stream)
                  case None => complete(NotFound)
                }
              case Failure(ex) => complete(InternalServerError, ex.getMessage)
            }
          }
        }
      }
    }
  }

  def processors: Route = {
    pathPrefix("streams/p/processors") {
      pathEndOrSingleSlash {
        get {
          onComplete((streamManager ? GetProcessors).mapTo[ProcessorCountResult]) {
            case Success(result) =>
              logger.info(s"Fetched processor count: ${result.processorCount}")
              complete(OK, result)
            case Failure(ex) =>
              logger.error(s"Failure: ${ex.getMessage}")
              complete(InternalServerError, ex.getMessage)
          }
        }
      }
    }
  }
}
