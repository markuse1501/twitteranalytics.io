package io.twitteranalytics.stream.db

import com.github.tototoshi.slick.MySQLJodaSupport._
import io.twitteranalytics.stream.TwitterStream
import org.joda.time.DateTime
import slick.jdbc.MySQLProfile.api._

private[db] trait TwitterStreamTable {

  class InputStreams(tag: Tag) extends Table[TwitterStream](tag, "streams") {
    def hash = column[String]("hash", O.PrimaryKey, O.Unique, O.SqlType("VARCHAR(32) NOT NULL UNIQUE"))
    def name = column[String]("name", O.SqlType("VARCHAR(100) NOT NULL"))
    def consumerKey = column[String]("consumer_key", O.SqlType("VARCHAR(100) NOT NULL"))
    def consumerSecret = column[String]("consumer_secret", O.SqlType("VARCHAR(100) NOT NULL"))
    def accessToken = column[String]("access_token", O.SqlType("VARCHAR(100) NOT NULL"))
    def accessTokenSecret = column[String]("access_token_secret", O.SqlType("VARCHAR(100) NOT NULL"))
    def isRunning = column[Boolean]("is_running", O.SqlType("TINYINT NOT NULL"))
    def created = column[DateTime]("created_at", O.SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))
    def lastModified = column[DateTime]("last_modified", O.SqlType("TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP"))
    def * = (hash.?, name, consumerKey, consumerSecret, accessToken, accessTokenSecret, isRunning, created, lastModified) <> ((TwitterStream.apply _).tupled, TwitterStream.unapply)
  }
  protected val streams = TableQuery[InputStreams]
}
