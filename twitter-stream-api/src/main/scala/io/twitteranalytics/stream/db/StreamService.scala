package io.twitteranalytics.stream.db

import java.util.UUID

import akka.http.scaladsl.model.{StatusCode, StatusCodes}
import com.typesafe.scalalogging.LazyLogging
import io.twitteranalytics.stream.{CreateStreamRequest, TwitterStream, UpdateStreamRequest}
import javax.inject.Singleton
import org.joda.time.DateTime

import scala.concurrent.{ExecutionContext, Future}

case class StreamServiceException(msg: String, statusCode: StatusCode) extends RuntimeException(msg)

@Singleton
class StreamService(private val storage: StreamRepository)
                   (implicit ec: ExecutionContext) extends LazyLogging {

  def getStreams: Future[Iterable[TwitterStream]] = storage.getAll

  def getStream(hash: String): Future[Option[TwitterStream]] = storage.getByHash(hash)

  def createStream(request: CreateStreamRequest): Future[TwitterStream] = {
    storage.save(
      TwitterStream(Some(UUID.randomUUID().toString.replace("-", "")),
        request.name, request.consumerKey, request.consumerSecret,
        request.accessToken, request.accessTokenSecret, createdAt = DateTime.now(), lastModified = DateTime.now())
    )
  }

  def updateStream(hash: String, request: UpdateStreamRequest): Future[TwitterStream] = {
    if (request.hash.isEmpty) {
      logger.error("Empty request hash")
      throw new StreamServiceException("Invalid request hashes", StatusCodes.BadRequest)
    }
    if (request.hash != hash) {
      logger.error("Hashes doesn't match")
      throw new StreamServiceException(s"Path($hash) and request(${request.hash}) hashes doesn't match",
        StatusCodes.BadRequest)
    }

    getStream(request.hash).flatMap {
      case Some(existing) =>
        storage.save(existing.copy(
          name = request.name,
          consumerKey = request.consumerSecret,
          consumerSecret = request.consumerSecret,
          accessToken = request.accessToken,
          accessTokenSecret = request.accessTokenSecret,
          lastModified = DateTime.now()))
      case None =>
        logger.error(s"No stream '$hash' found")
        throw new StreamServiceException(s"Stream '${request.hash}' not found", StatusCodes.NotFound)
    }
  }

  def deleteStream(hash: String): Future[Int] = storage.delete(hash)

  def setRunning(hash: String): Future[Option[TwitterStream]] = {
    getStream(hash).flatMap {
      case Some(stream) =>
        logger.info(s"Set status to RUNNING in stream: ${stream.name}")
        storage.save(stream.copy(isRunning = true)).flatMap { _ => storage.getByHash(hash) }
      case None =>
        logger.error(s"Stream '$hash' not found. Unable to set status to RUNNING")
        Future.successful(None)
      //throw new StreamServiceException(s"Stream '$hash' not found. Unable to set status to RUNNING", StatusCodes.NotFound)
    }
  }
}
