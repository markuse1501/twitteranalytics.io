package io.twitteranalytics.stream.db

import io.twitteranalytics.stream.TwitterStream
import slick.jdbc.JdbcBackend.Database

import scala.concurrent.{ExecutionContext, Future}

case class SaveStreamRequest(hash: Option[String], name: String, consumerKey: String,
                             consumerSecret: String, accessToken: String, accessTokenSecret: String)

trait StreamRepository {

  def getAll: Future[Iterable[TwitterStream]]

  def getByHash(hash: String): Future[Option[TwitterStream]]

  def save(stream: TwitterStream): Future[TwitterStream]

  def delete(hash: String): Future[Int]
}

class MariaDBRepository(private val db: Database)
                       (implicit executionContext: ExecutionContext)
  extends TwitterStreamTable with StreamRepository {

  import slick.jdbc.MySQLProfile.api._

  def getAll: Future[Iterable[TwitterStream]] = {
    db.run(streams.result)
  }

  def getByHash(hash: String): Future[Option[TwitterStream]] =
    db.run(streams.filter(_.hash === hash).result.headOption)

  def save(stream: TwitterStream): Future[TwitterStream] = {
    db.run(streams.insertOrUpdate(stream)).map(_ => stream)
  }

  def delete(hash: String): Future[Int] = {
    db.run(streams.filter(_.hash === hash).delete)
  }
}