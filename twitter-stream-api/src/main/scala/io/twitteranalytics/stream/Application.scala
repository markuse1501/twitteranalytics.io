package io.twitteranalytics.stream

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.stream.Materializer
import com.google.inject.Guice
import com.google.inject.name.Names
import com.typesafe.scalalogging.LazyLogging
import io.twitteranalytics.stream.actor.StreamSupervisor
import io.twitteranalytics.stream.guice.ApplicationModule
import io.twitteranalytics.stream.http.HttpRoutes
import net.codingwell.scalaguice.InjectorExtensions.ScalaInjector

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object Application extends App with LazyLogging {

  import com.typesafe.config.ConfigFactory

  logger.info("Starting 'twitter-stream-api'")

  private val config = ConfigFactory.load()
  private val httpConfig = config.getConfig("http")

  private val host = Option(httpConfig.getString("host")).getOrElse("0.0.0.0")
  private val port = Option(httpConfig.getInt("port")).getOrElse(5000)

  logger.info(s"Loaded http configuration: $host:$port")

  private val injector: ScalaInjector = new ScalaInjector(Guice.createInjector(new ApplicationModule))

  object ActorUtil {
    implicit class ActorInjector(injector: ScalaInjector)  {
      def getActor(name: String) : ActorRef = {
        injector.instance[ActorRef](Names.named(name))
      }
    }
  }

  import ActorUtil._

  private implicit val actorSystem= injector.instance[ActorSystem]
  private implicit val materializer = injector.instance[Materializer]
  private implicit val executionContext = injector.instance[ExecutionContext]

  private val streamSupervisor = injector.getActor(StreamSupervisor.name)

  private val httpRoutes = injector.instance[HttpRoutes]

  private val serverBindingFuture = Http().bindAndHandle(httpRoutes.all, host, port)

  serverBindingFuture.map { serverBinding =>
    logger.info(s"Trying to bind Application to ${serverBinding.localAddress}")
  }.onComplete {
    case Success(_) => logger.info("Server successful bound")
    case Failure(ex) =>
      logger.error("Failed to bind {}:{}: {}", host, port, ex.getMessage)
      actorSystem.terminate()
  }
}
