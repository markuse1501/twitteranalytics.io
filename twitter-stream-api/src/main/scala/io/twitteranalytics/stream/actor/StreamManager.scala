package io.twitteranalytics.stream.actor

import java.util.concurrent.ConcurrentHashMap

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.http.scaladsl.model.StatusCodes
import akka.stream.Materializer
import io.twitteranalytics.stream.TwitterStream
import io.twitteranalytics.stream.db.{StreamService, StreamServiceException}
import javax.inject.Inject

import scala.concurrent.ExecutionContext

object StreamManager {

  def name = "stream-manager"

  def props(streamService: StreamService)
           (implicit system: ActorSystem, ec: ExecutionContext, mat: Materializer): Props =
    Props(new StreamManager(streamService))

  sealed trait ManagerCommand
  final case class CreateStream(stream: TwitterStream) extends ManagerCommand
  final case class StartStream(hash: String) extends ManagerCommand
  final case class StopStream(hash: String) extends ManagerCommand
  final case object GetProcessors extends ManagerCommand

  final case class StreamCommand(hash: String, action: String) extends ManagerCommand

  
  final case class ProcessorCountResult(processorCount: Int)

}

class StreamManager @Inject()(streamService: StreamService)
                             (implicit system: ActorSystem, ec: ExecutionContext, mat: Materializer)
  extends Actor with ActorLogging {

  import StreamManager._

  val streamHashProcessorMapping = new ConcurrentHashMap[String, StreamProcessor]()


  //TODO
  //  override def supervisorStrategy: SupervisorStrategy = {
  //    case _: ActorKilledException => Escalate
  //    case _: ActorInitializationException => Escalate
  //    case _ => Restart // keep restarting faulty actor
  //  }

  override def preStart(): Unit = {
    log.info(s"preStart() $name, populating internal storage")
    streamService.getStreams.foreach(streams => {
      streams.foreach(stream => {
        log.info(s"Store internal: ${stream.name}")
        val processor = new StreamProcessor(stream)
        streamHashProcessorMapping.put(stream.hash.get, processor)
        if (stream.isRunning) {
          processor.start()
        }
      })
    })
  }

  override def preRestart(reason: Throwable, message: Option[Any]): Unit = {
    log.info(s"preRestart() $name, stopping all processors")
    streamHashProcessorMapping.values().stream().forEach(processor => {
      log.info(s"Stop processor: ${processor.name}")
      processor.stop(withShutdown = true)
    })
  }

  override def receive: Receive = {

    case CreateStream(stream) =>
      log.info(s"Received CreateStream(${stream.hash})")
      val processor = new StreamProcessor(stream)
      log.info(s"Created processor: ${processor.name}")
      stream.hash match {
        case Some(hash) =>
          log.info("Add processor to internal storage")
          streamHashProcessorMapping.put(hash, processor)
          sender() ! hash
        case None => //TODO Try
          val ex = new StreamServiceException(s"Hash is empty. Unable to create stream '${stream.name}'",
            StatusCodes.BadRequest)
          log.error(s"Throwing exception: ${ex.getMessage}")
          throw ex
      }

    case StartStream(hash) =>
      log.info(s"Received StartStream($hash)")

      val existing = Option(streamHashProcessorMapping.get(hash))

      if (existing.isEmpty) {
        val ex = new StreamServiceException(s"No stream '$hash in internal storage found'", StatusCodes.BadRequest)
        log.error(s"Throwing exception: ${ex.getMessage}")
        throw ex
      } else {
        val processor = existing.get
        processor.start()

        //TODO Await?
        streamService.setRunning(hash).map { stream =>
          sender() ! stream
        }
      }

    case StopStream(hash) =>
      log.info(s"Stopping stream '$hash'")

      val existing = Option(streamHashProcessorMapping.get(hash))

      if (existing.isEmpty) {
        val ex = new StreamServiceException(s"No stream '$hash in internal storage found'", StatusCodes.BadRequest)
        log.error(s"Throwing exception: ${ex.getMessage}")
        throw ex
      } else {
        val processor = existing.get
        processor.stop(withShutdown = false)

        //TODO Await?
        streamService.setRunning(hash).map { stream =>
          sender() ! stream
        }
      }


    case GetProcessors => {
      log.info(s"Received GetProcessors")
      sender() ! ProcessorCountResult(streamHashProcessorMapping.size())
    }


    case msg: Any =>
      log.info(s"Unknown message '$msg' received")
  }
}
