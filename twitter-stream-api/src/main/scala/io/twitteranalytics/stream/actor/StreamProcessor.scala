package io.twitteranalytics.stream.actor

import akka.actor.{ActorSystem, PoisonPill}
import akka.event.{Logging, LoggingAdapter}
import akka.kafka.ProducerSettings
import akka.kafka.scaladsl.Producer
import akka.stream.Materializer
import com.typesafe.scalalogging.LazyLogging
import io.twitteranalytics.stream.TwitterStream
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.{ByteArraySerializer, StringSerializer}
import org.joda.time.DateTime

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

class StreamProcessor(private val stream: TwitterStream)
                     (implicit system: ActorSystem, ec: ExecutionContext, mat: Materializer) extends LazyLogging {


  //akka-stream log
  private implicit val adapter: LoggingAdapter = Logging(system, classOf[StreamProcessor])

  val producerSettings = ProducerSettings(system, new ByteArraySerializer, new StringSerializer)


  val tweetStreamListener = new TweetStreamListener(TwitterStream(Some("hash123"), "mystream", "cpZFK9mi4gzxm9xemo7EgHkH4",
    "pG8BKZOvwDozwI4T9rvTaNLQDWKu0OesLdJwlDSMtsmRvBoeq9", "41811970-zm59XPpyUIPKRXwIkML8ZJT1dMV7szFmvyMo5aaEH",
    "Pw76L8wYmy1SnlNAiIyBGOvx7QAoKn549wMTMNaE0Vxqy", isRunning = false, DateTime.now(), DateTime.now()))

  val (tweetSource, sourceActorRef) = tweetStreamListener.sourceWithActorRef()

  def name = stream.name

  def start(): Unit = {
    logger.info(s"Starting Stream: ${stream.name}")
    tweetSource
      .map { tweet => tweet.text }
      .log("tweet-pipeline", { tweet => tweet })
      .map { tweet => new ProducerRecord[Array[Byte], String]("raw_tweets", tweet) }
      .runWith(Producer.plainSink(producerSettings))
      .onComplete {
        case Success(_) => logger.info("Pipeline done start successful")
        case Failure(ex) => logger.error(s"Pipeline done start with failure: ${ex.getMessage}")
      }
  }

  def stop(withShutdown: Boolean): Unit = {
    logger.info(s"Stopping Stream: ${stream.name}")
    tweetStreamListener.cleanUp(withShutdown)
    logger.info(s"Sending PoisonPill to sourceActorRef '${sourceActorRef.path}'")
    sourceActorRef ! PoisonPill

  }
}