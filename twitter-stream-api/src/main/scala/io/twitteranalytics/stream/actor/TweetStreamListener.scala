package io.twitteranalytics.stream.actor

import akka.NotUsed
import akka.actor.{ActorRef, ActorSystem}
import akka.stream.scaladsl.{Keep, Sink, Source}
import akka.stream.{Materializer, OverflowStrategy}
import com.typesafe.scalalogging.LazyLogging
import io.twitteranalytics.stream.TwitterStream
import twitter4j._
import twitter4j.conf.ConfigurationBuilder

import scala.concurrent.ExecutionContext

case class TweetInfo(text: String, author: String)

class TweetStreamListener(stream: TwitterStream)
                         (implicit system: ActorSystem, mat: Materializer, ec: ExecutionContext)
  extends LazyLogging {

  private val config = new ConfigurationBuilder()
    .setOAuthConsumerKey(stream.consumerKey)
    .setOAuthConsumerSecret(stream.consumerSecret)
    .setOAuthAccessToken(stream.accessToken)
    .setOAuthAccessTokenSecret(stream.accessTokenSecret)
    .build

  val twitterStream = new TwitterStreamFactory(config).getInstance

  def sourceWithActorRef(): (Source[TweetInfo, NotUsed], ActorRef) = {

    val (actorRef, publisher) = Source.actorRef[TweetInfo](1000, OverflowStrategy.fail)
      .toMat(Sink.asPublisher(false))(Keep.both).run()

    val listener = new UserStreamAdapter {

      override def onStatus(status: Status): Unit = {
        actorRef ! TweetInfo(status.getText, status.getUser.getName)
      }

      override def onException(ex: Exception): Unit = {
        logger.warn(s"Stream[${stream.name}], onException(): ${ex.getMessage}")
      }

      override def onDeletionNotice(statusDeletionNotice: StatusDeletionNotice): Unit = {
        logger.info(s"Stream[${stream.name}], onDeletionNotice(): statusId=${statusDeletionNotice.getStatusId} userId=${statusDeletionNotice.getUserId}")
      }

      override def onTrackLimitationNotice(numberOfLimitedStatuses: Int): Unit = {
        logger.info(s"Stream[${stream.name}], onTrackLimitationNotice(): numberOfLimitedStatuses=$numberOfLimitedStatuses")
      }

      override def onStallWarning(warning: StallWarning): Unit = {
        logger.info(s"Stream[${stream.name}], onStallWarning(): ${warning.getMessage}")
      }

      override def onFavorite(source: User, target: User, favoritedStatus: Status): Unit = {
        //logger.info(s"Stream[${stream.name}], onFavorite(): source=${source.getName}, target=${target.getName}, favoritedStatus=${favoritedStatus.getText}")
      }

      override def onFollow(source: User, followedUser: User): Unit = {
        //logger.info(s"Stream[${stream.name}], onFollow(): source=${source.getName}, followedUser=${followedUser.getName}")
      }
    }
    twitterStream.addListener(listener)
    twitterStream.user()

    (Source.fromPublisher(publisher), actorRef)
  }

  def cleanUp(withShutdown: Boolean): Unit = {
    logger.info("Cleanup Twitter stream listener and thread")
    twitterStream.clearListeners()
    twitterStream.cleanUp()
    if(withShutdown) twitterStream.shutdown()
  }
}
