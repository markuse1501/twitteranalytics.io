package io.twitteranalytics.stream.actor

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props, Terminated}
import akka.stream.Materializer
import io.twitteranalytics.stream.db.StreamService

import scala.concurrent.ExecutionContext

object StreamSupervisor {

  def name = "stream-supervisor"

  def props(streamService: StreamService)
           (implicit system: ActorSystem, ec: ExecutionContext, mat: Materializer): Props =
    Props(new StreamSupervisor(streamService))
}

class StreamSupervisor(streamService: StreamService)
                      (implicit val system: ActorSystem, ec: ExecutionContext, mat: Materializer)
  extends Actor with ActorLogging {

  //TODO
  //  override def supervisorStrategy: SupervisorStrategy = {
  //    case _: ActorKilledException => Escalate
  //    case _: ActorInitializationException => Escalate
  //    case _ => Restart // keep restarting faulty actor
  //  }

  var streamManager: ActorRef = _

  override def preStart(): Unit = {
    log.info(s"preStart() StreamSupervisor")
    streamManager = context.actorOf(StreamManager.props(streamService), StreamManager.name)
    log.info(s"Created StreamManager actor: ${streamManager.path}")
    context.watch(streamManager)
  }

  override def preRestart(reason: Throwable, message: Option[Any]): Unit = {
    log.error(s"preRestart() StreamSupervisor because of exception: ${reason.getMessage}")
    context.unwatch(streamManager)
  }

  override def receive: Receive = {
    case Terminated(actor) =>
      log.error(s"Stream manager '${actor.path}' terminated")
      streamManager = context.actorOf(StreamManager.props(streamService), StreamManager.name)
  }
}
