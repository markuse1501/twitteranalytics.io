package io.twitteranalytics

import org.joda.time.DateTime

package object stream {

  /** Simple Twitter input stream db entity */
  case class TwitterStream(hash: Option[String], name: String,
                           consumerKey: String, consumerSecret: String,
                           accessToken: String, accessTokenSecret: String,
                           isRunning: Boolean = false, createdAt: DateTime,
                           lastModified: DateTime) {

    override def toString = s"TwitterStream[hash=${hash.getOrElse("")}, name=$name, isRunning=$isRunning]"
  }

  //TODO just use one
  case class CreateStreamRequest(name: String, consumerKey: String,
                                 consumerSecret: String, accessToken: String,
                                 accessTokenSecret: String) {

    override def toString = s"CreateStreamRequest[name=$name]"
  }

  case class UpdateStreamRequest(hash: String, name: String, consumerKey: String,
                                 consumerSecret: String, accessToken: String,
                                 accessTokenSecret: String) {

    override def toString = s"UpdateStreamRequest[hash=$hash, name=$name]"
  }

  case class StreamProcessorRequest(hash: String, action: String)

}
