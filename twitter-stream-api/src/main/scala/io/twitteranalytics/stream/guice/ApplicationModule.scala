package io.twitteranalytics.stream.guice
import com.google.inject.AbstractModule
import net.codingwell.scalaguice.ScalaModule

class ApplicationModule extends AbstractModule with ScalaModule {

  override def configure(): Unit = {
    install(new AkkaModule)
    install(new StreamModule)
    install(new HttpModule)
  }
}
