package io.twitteranalytics.stream.guice

import akka.actor.ActorSelection
import com.google.inject.name.Named
import com.google.inject.{AbstractModule, Provides}
import com.typesafe.scalalogging.LazyLogging
import io.twitteranalytics.stream.db.StreamService
import io.twitteranalytics.stream.http.TwitterStreamApi
import javax.inject.Singleton
import net.codingwell.scalaguice.ScalaModule

import scala.concurrent.ExecutionContext

class HttpModule extends AbstractModule with ScalaModule with LazyLogging{

  override def configure(): Unit = {
  }

  @Singleton
  @Provides
  def streamsRestApi(streamService: StreamService, @Named("stream-manager") streamManager: ActorSelection)
                    (implicit executionContext: ExecutionContext): TwitterStreamApi = {
    logger.info("Creating TwitterStreamApi")
    new TwitterStreamApi(streamService, streamManager)
  }
}
