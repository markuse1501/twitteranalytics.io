package io.twitteranalytics.stream.guice

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, Materializer}
import com.google.inject.{AbstractModule, Provides}
import com.typesafe.scalalogging.LazyLogging
import javax.inject.Singleton
import net.codingwell.scalaguice.ScalaModule

import scala.concurrent.ExecutionContext

class AkkaModule extends AbstractModule with ScalaModule with LazyLogging {

  override def configure(): Unit = {
    logger.info("Creating ActorSystem")
    bind[ActorSystem].toInstance(ActorSystem("twitter-stream-api"))
  }

  @Singleton
  @Provides
  def executionContext(implicit system: ActorSystem): ExecutionContext = {
    logger.info("Creating ExecutionContext")
    system.dispatcher
  }

  @Singleton
  @Provides
  def actorMaterializer(implicit system: ActorSystem): Materializer = {
    logger.info("Creating ActorMaterializer")
    ActorMaterializer()
  }
}