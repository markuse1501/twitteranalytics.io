package io.twitteranalytics.stream.guice

import akka.actor.{ActorRef, ActorSelection, ActorSystem}
import akka.stream.Materializer
import com.google.inject.name.Named
import com.google.inject.{AbstractModule, Provides}
import com.typesafe.scalalogging.LazyLogging
import io.twitteranalytics.stream.actor.{StreamManager, StreamSupervisor}
import io.twitteranalytics.stream.db.{MariaDBRepository, StreamRepository, StreamService}
import javax.inject.Singleton
import net.codingwell.scalaguice.ScalaModule

import scala.concurrent.ExecutionContext

class StreamModule extends AbstractModule with ScalaModule with LazyLogging {

  import slick.jdbc.JdbcBackend.Database

  override def configure(): Unit = {
    bind[Database].toInstance(Database.forConfig("streams-db"))
  }

  @Singleton
  @Provides
  def streamRepository(db: Database)(implicit ec: ExecutionContext): StreamRepository = {
    logger.info("Creating StreamRepository, resolved Database dependency")
    new MariaDBRepository(db)
  }

  @Singleton
  @Provides
  def streamService(storage: StreamRepository)
                   (implicit ec: ExecutionContext): StreamService = {
    logger.info("Creating StreamService")
    new StreamService(storage)
  }

  @Singleton
  @Provides
  @Named("stream-supervisor")
  def streamSupervisor(streamService: StreamService)
                      (implicit system: ActorSystem, ec: ExecutionContext, mat: Materializer): ActorRef = {
    val supervisor = system.actorOf(StreamSupervisor.props(streamService), StreamSupervisor.name)
    logger.info(s"Creating StreamSupervisor '${supervisor.path}'")
    supervisor
  }

  @Singleton
  @Provides
  @Named("stream-manager")
  def streamManager(implicit system: ActorSystem): ActorSelection = {
    logger.info("Creating StartManager actor selection")
    system.actorSelection(s"akka://${system.name}/user/${StreamSupervisor.name}/${StreamManager.name}")
  }
}
