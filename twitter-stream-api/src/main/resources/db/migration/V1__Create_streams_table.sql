CREATE TABLE `streams` (
  `hash`                VARCHAR(32)  NOT NULL UNIQUE,
  `name`                VARCHAR(100) NOT NULL,
  `consumer_key`        VARCHAR(100) NOT NULL,
  `consumer_secret`     VARCHAR(100) NOT NULL,
  `access_token`        VARCHAR(100) NOT NULL,
  `access_token_secret` VARCHAR(100) NOT NULL,
  `is_running`          TINYINT(1)   NOT NULL DEFAULT '0',
  `created_at`          TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified`       TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`hash`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE INDEX idx_stream_hash
  ON streams (hash);

INSERT INTO `streams` VALUES ('bd99b075552c43b3b992c7e5a803a699', 'Timeline stream', 'ck1', 'cs1', 'at1', 'ats1', 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO `streams` VALUES ('13b2d1c7290c4bb4934d042340439b9b', 'Akka hashtag', 'ck2', 'cs2', 'at2', 'ats2', 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
