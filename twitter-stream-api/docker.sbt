dockerBaseImage       := "anapsix/alpine-java:8_jdk"
packageName in Docker := "twitter-stream-api"
dockerExposedPorts := Seq(5000)